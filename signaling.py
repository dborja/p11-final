import asyncio
import json
import formatoMensaje
import argparse

clientlist = []
streamers = {}
ficheros = []
mensaje_no_enviado = []
streamer_elegido = ""


class EchoServerProtocol:

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))

        if message.split("-")[0] == "REGISTER STREAMER":
            for streamer in json.loads(message.split("-")[1]).keys():
                streamers[streamer] = addr
            ficheros.append(message.split("-")[1])
            formatoMensaje.log_message("Mensaje REGISTRO STREAMER recibido de " + str(addr))
            print(streamers)

        if message == "LISTA":
            clientlist.append({"Nombre": len(clientlist) + 1, "Direccion": addr})
            print('Send %r to %s' % (ficheros, addr))
            self.transport.sendto(json.dumps(ficheros).encode(), addr)

        if message.split(":")[0] == "Name":
            global streamer_elegido
            streamer_elegido = message.split(":")[1]

        if message.split('"')[len(message.split('"')) - 2] == "offer":
            formatoMensaje.log_message("Mensaje de oferta SDP recibido de" + str(addr))
            self.transport.sendto(message.encode(), streamers[streamer_elegido])
            formatoMensaje.log_message("Mensaje de oferta SDP enviado a " + str(streamers[streamer_elegido]))

        if message.split('"')[len(message.split('"')) - 2] == "answer":
            formatoMensaje.log_message("Mensaje de respuesta SDP recibido de " + str(addr))
            print('Send %r to %s' % (message, clientlist[len(clientlist) - 1]["Direccion"]))
            self.transport.sendto(message.encode(), clientlist[len(clientlist) - 1]["Direccion"])
            formatoMensaje.log_message(
                "Mensaje de respuesta SDP enviada a" + str(clientlist[len(clientlist) - 1]["Direccion"]))


async def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("signal_port", type=int, help="UDP port to listen for signaling messages")
    args = parser.parse_args()
    port = args.signal_port
    formatoMensaje.log_message("Comienzo")
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', port))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


if __name__ == "__main__":
    asyncio.run(main())
