# ENTREGA CONVOCATORIA JUNIO
Daniel Borja Fernández d.borja.2021@alumnos.urjc.es

## Parte básica
El funcionamiento de la practica se ha hecho utizando google chrome ya que al utilizar firefox el funcionamiento es 
correcto pero no se visualiza el video en el navegador.

## Parte adicional

* Información adicional para los ficheros

	- No se modifica la manera de ejecutal los programas.
	Se añade junto al titulo del video que ofrece cada streamer una pequeña descripción de lo que se muestra.
	
* Funcionamiento entre distintas máquinas.
	
    - Ejecutamos signaling.py en una máquina pasando el argumento signal_port.
    - Ejecutamos streamer.py en otra máquina con los argumentos video_file, signal_ip y signal_port (ip y puerto de la 
  maquina en la que ejecutamos signalin.py. En el caso del ejemplo 212.128.255.29 y 7000).
    - Ejecutamos front.py en una tercera máquina con los argumentos del http_port, signal_ip y signal_port (ip y puerto 
  de la maquina en la que ejecutamos signalin.py. En el caso del ejemplo 212.128.255.29 y 7000).
  
  
  Video explicativo: https://www.youtube.com/watch?v=F80-TGXt44c
